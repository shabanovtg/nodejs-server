const PORT = 8138;

const apps = new Array(2).fill(1).map((n, i) => {
  const _PORT = PORT + i * 2;

  return {
    name: 'nodejs_server',
    exec_mode: 'fork',
    script: './index.js',
    log_date_format: 'YYYY-MM-DD HH:mm',
    env: {
      NODE_ENV: 'production',
      PORT: _PORT,
    },
    args: ['--color'],
  };
});

module.exports = {
  apps,
};
