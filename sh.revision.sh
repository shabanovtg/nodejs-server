#!/bin/bash
#set -e
script="$0"
config1=".revision.log"
basename="$(dirname $script)"

echo Saving params for "$(cd "$(dirname "$0")"; pwd -P)/$(basename "$0")"
cd "$basename"
echo "------------------------------------------------" >> "$config1";
date >> "$config1";
echo "$1" >> "$config1"; #comment
echo "repo commit:" >> "$config1";
git rev-parse HEAD >> "$config1";
echo "packages commit:" >> "$config1";
result=$(grep _resolved node_modules/youdrive-packages/package.json)
echo "$result" >> "$config1";
echo "------------------------------------------------" >> "$config1";
echo Saved
tail "$config1"
