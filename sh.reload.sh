#!/usr/bin/env bash
set -e
echo For reloading project will be executed:
echo 1 save revision
echo 2 pull repository
echo 3 npm install
echo 4 restart api

for (( i=5; i>0; i--)); do
  sleep 1 &
  printf " Ctrl+C to cancel reloading project: $i \r"
  wait
done

script="$0"
basename="$(dirname $script)"
cd "$basename"

echo Start script "$(cd "$(dirname "$0")"; pwd -P)/$(basename "$0")"

echo '**** revision.1'
sh sh.revision.sh beforepull;
echo '**** fetch/pull'
echo '*started'
git fetch;
git pull;
echo '**** npm i'
echo '*started'
npm i;
echo '**** revision.2'
echo '*started'
sh sh.revision.sh afterpull
#echo '**** test'
#NODE_ENV=test npm run test;
echo '**** api restart'
echo '*started'
pm2 reload nodejs_server;
echo '**** finish'
