const request = require('supertest');
const app = require('../index.js');

describe('GET /', function() {
  it('respond with connection success', function(done) {
    request(app).get('/').expect('connection success', done);
  });
});
