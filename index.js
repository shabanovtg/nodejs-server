const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 8138;
const startedAt = new Date().toString();

process.on('uncaughtException', err => {
  console.error(`uncaughtException[${new Date().toString()}]:`, err.message);
  console.log(err.stack);
});

process.on('unhandledRejection', (reason, promise) => {
  console.error(
    `unhandledRejection[${new Date().toString()}]:`,
    reason,
    promise,
  );
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', ({query, body, params}, response) => {
  console.log('get', query, body, params);
  response.send(`connection success to ${port} ${__dirname} ${startedAt}`);
});

app.post('/*', ({query, body, params}, res) => {
  console.log('post', query, body, params);
  res.send({
    code: 0,
  });
});

app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err);
  }
  console.log(`server is listening on ${port}`);
});

module.exports = app;
